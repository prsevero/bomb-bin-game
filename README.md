# Bomb Bin Game

## Installing dependencies
`npm install`

## Running tests
`npm test`

## Running locally
`npm start`

## Building for production
`npm run build`

## Running for production
`npm run serve`

## Running game
### Development mode
After installing the dependencies, you can just run `npm start`.
### Production mode
After installing the dependencies, you need to run `npm run build`. After it's built you can run `npm run serve` and access the game [here](http://localhost:3001).
