const randomBetweenValues = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

const FUNCTIONS = {
    randomBetweenValues: randomBetweenValues,
}

export default FUNCTIONS;
