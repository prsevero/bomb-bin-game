import React from 'react';
import {mount} from 'enzyme';
import App from './app';


describe('App Component Tests', () => {

    let props;
    let mountedApp;
    const app = () => {
        if (!mountedApp) {
            mountedApp = mount(<App {...props} />);
        }
        return mountedApp;
    }

    beforeEach(() => {
        mountedApp = undefined;
    });


    // All tests will go here
    it('always renders the app wrapper section', () => {
        const wrapper = app().find('section#app-wrapper');
        expect(wrapper.length).toBe(1);
    });

    it('always renders the bins wrapper section', () => {
        const wrapper = app().find('section#bins-wrapper');
        expect(wrapper.length).toBe(1);
    });

    it('always renders three bins', () => {
        const bins = app().find('.bin');
        expect(bins.length).toBe(3);
    });

    it('always renders the bins counter', () => {
        const binsCounter = app().find('#bins-counter');
        expect(binsCounter.length).toBe(1);
    });

    it('always renders the bombs wrapper', () => {
        const wrapper = app().find('#bombs-wrapper');
        expect(wrapper.length).toBe(1);
    });

    it('always call bins change and place bombs timers', () => {
        jest.useFakeTimers();
        app();
        expect(setTimeout).toHaveBeenCalledTimes(2);
    });

    it('always renders the reset button', () => {
        const wrapper = app().find('section#reset-button');
        expect(wrapper.length).toBe(1);
        expect(wrapper.find('button').length).toBe(1);
    });

});
