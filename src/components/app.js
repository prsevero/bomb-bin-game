import React from 'react';

import CONSTANTS from '../constants';
import Bomb from './bomb';
import './app.css';


let initialState = {
    binsColors: {
        real: CONSTANTS.colors,
        visible: CONSTANTS.light_colors,
        percentage: CONSTANTS.light_colors_percentages,
    },
    binsSizes: [CONSTANTS.bins_scale_size, CONSTANTS.bins_scale_size, CONSTANTS.bins_scale_size],
    binsTimer: CONSTANTS.bins_timer,
    binsTickTimer: undefined,
    bombsPlaced: [],
    placeBombTimer: undefined,
    score: 0,
    status: 'playing',
};


class App extends React.Component {
    constructor (props) {
        super(props);

        initialState.binsColors.real = this.drawBinsColors();
        this.state = initialState;
    }

    componentDidMount () {
        this.getBinsPositions();
        this.binsTick();
        this.placeBomb();
    }


    resetGame = () => {
        clearTimeout(this.state.binsTickTimer);
        clearTimeout(this.state.placeBombTimer);
        initialState.bombsPlaced = [];
        this.setState(initialState, () => {
            this.getBinsPositions();
            this.binsTick();
            this.placeBomb();
        });
    }


    endGame () {
        this.setState({status: 'ended'});
    }


    getElementPosition (elem) {
        let left = 0, top = 0;

        if (elem) {
            left = elem.offsetLeft;
            top = elem.offsetTop;
            if (elem.offsetParent) {
                left += elem.offsetParent.offsetLeft;
                top += elem.offsetParent.offsetTop;
            }
        }

        return {
            bottom: top + elem.offsetHeight,
            left: left,
            right: left + elem.offsetWidth,
            top: top,
        };
    }


    // Bins Functions
    getBinsPositions () {
        let binOne, binTwo, binThree;
        binOne = this.getElementPosition(this.binOne);
        binTwo = this.getElementPosition(this.binTwo);
        binThree = this.getElementPosition(this.binThree);

        this.setState({
            binsPositions: [binOne, binTwo, binThree],
        });
    }

    drawBinsColors () {
        let realColors = this.state ? this.state.binsColors.real : undefined,
            lastColors = realColors ? [realColors[0], realColors[1], realColors[2]] : undefined;
        for (let i=CONSTANTS.colors.length-1; i>0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            [CONSTANTS.colors[i], CONSTANTS.colors[j]] = [CONSTANTS.colors[j], CONSTANTS.colors[i]];
        }

        if (
            lastColors !== undefined &&
            lastColors[0] === CONSTANTS.colors[0] &&
            lastColors[1] === CONSTANTS.colors[1] &&
            lastColors[2] === CONSTANTS.colors[2]
        ) this.drawBinsColors();

        return CONSTANTS.colors;
    }

    getBinColor (binColor) {
        return this.state.binsColors.visible[binColor].replace('{}', this.state.binsColors.percentage[binColor]);
    }

    binsTick () {
        let binsTickTimer = setTimeout(() => {
            let binsTimer = this.state.binsTimer;
            if (binsTimer <= 1) {
                binsTimer = CONSTANTS.bins_timer;
                this.drawBinsColors();
            } else binsTimer--;

            this.setState({binsTimer: binsTimer});
            if (this.state.status !== 'ended') this.binsTick();
        }, 1000);
        this.setState({binsTickTimer: binsTickTimer});
    }

    changeBinShade (color) {
        let binsColors = this.state.binsColors;
        binsColors.percentage[color] += CONSTANTS.bins_shade_factor;
        this.setState({binsColors: binsColors});
    }

    increaseBinSize (index) {
        let sizes = this.state.binsSizes;
        sizes[index] += CONSTANTS.bins_scale_factor;
        this.setState({binsSizes: sizes});
    }


    // Bombs Functions
    placeBomb () {
        let bombsPlaced = this.state.bombsPlaced;

        let placeBombTimer = setTimeout(() => {
            bombsPlaced.push(
                <Bomb explodeBomb={this.explodeBomb}
                    handleDrop={this.dropBomb}
                    index={bombsPlaced.length}
                    key={bombsPlaced.length} />
                );
            this.setState({bombsPlaced: bombsPlaced});

            if (bombsPlaced.length < CONSTANTS.bombs_total) this.placeBomb();
        }, CONSTANTS.bombs_timers[bombsPlaced.length]);
        this.setState({placeBombTimer: placeBombTimer});
    }

    dropBomb = (bomb, x, y, color) => {
        let position, placedInABin, index, max;
        x += CONSTANTS.bomb_size_drop;
        y += CONSTANTS.bomb_size_drop;

        for (index=0, max=this.state.binsPositions.length; index<max; index++) {
            position = this.state.binsPositions[index];
            if (x >= position.left && x <= position.right && y >= position.top && y <= position.bottom) {
                placedInABin = true;
                break;
            }
        }

        if (placedInABin) {
            let score = this.state.score,
                bombsPlaced = this.state.bombsPlaced;

            bombsPlaced[bomb.props.index] = undefined;

            if (color === this.state.binsColors.real[index]) {
                score++;
                this.changeBinShade(this.state.binsColors.real[index]);
                this.increaseBinSize(index);
            } else score--;

            this.setState({bombsPlaced: bombsPlaced, score: score});
        }

        if (this.state.bombsPlaced.length >= CONSTANTS.bombs_total) this.endGame();
    }

    explodeBomb = (bomb) => {
        let score = this.state.score,
            bombsPlaced = this.state.bombsPlaced;
        bombsPlaced[bomb.props.index] = undefined;
        score--;
        this.setState({bombsPlaced: bombsPlaced, score: score});
        if (this.state.bombsPlaced.length >= CONSTANTS.bombs_total) this.endGame();
    }


    render () {
        return (
            <section id="app-wrapper">
                <section id="bins-wrapper">
                    <div className="bin"
                        ref={(div) => { this.binOne = div; }}
                        style={{
                            backgroundColor: this.getBinColor(this.state.binsColors.real[0]),
                            transform: `scale(${this.state.binsSizes[0]})`
                        }} />
                    <div className="bin"
                        ref={(div) => { this.binTwo = div; }}
                        style={{
                            backgroundColor: this.getBinColor(this.state.binsColors.real[1]),
                            transform: `scale(${this.state.binsSizes[1]})`
                        }} />
                    <div className="bin"
                        ref={(div) => { this.binThree = div; }}
                        style={{
                            backgroundColor: this.getBinColor(this.state.binsColors.real[2]),
                            transform: `scale(${this.state.binsSizes[2]})`
                        }} />
                </section>

                {this.state.status === 'playing' ?
                    <section id="bins-counter">Change in: {this.state.binsTimer}s</section> : null}

                <section id="bombs-wrapper">
                    {this.state.bombsPlaced.map((bomb) => bomb)}
                </section>

                <section id="score">Score: {this.state.score}</section>
                <section id="reset-button"><button onClick={this.resetGame}>Reset</button></section>
            </section>
        );
    }
}

export default App;
