import React from 'react';
import Draggable from 'react-draggable';

import CONSTANTS from '../constants';
import FUNCTIONS from '../functions';
import './bomb.css';


class Bomb extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            color: CONSTANTS.colors[Math.floor(Math.random() * CONSTANTS.colors.length)],
            defaultPosition: this.drawPosition(),
            time: FUNCTIONS.randomBetweenValues(5, 10),
            timer: undefined,
        };
    }

    componentDidMount () {
        this.tick();
    }

    componentWillUnmount () {
        clearTimeout(this.state.timer);
    }

    drawPosition () {
        const scoreElem = document.getElementById('score'),
              binsWrapperElem = document.getElementById('bins-wrapper');

        return {
            x: FUNCTIONS.randomBetweenValues(0, window.innerWidth - CONSTANTS.bomb_size),
            y: FUNCTIONS.randomBetweenValues(
                scoreElem ? scoreElem.offsetHeight : 0,
                (binsWrapperElem ? binsWrapperElem.offsetTop : 0) - CONSTANTS.bomb_size
            )
        };
    }

    tick () {
        const timer = setTimeout(() => {
            let time = this.state.time;
            time--;

            if (time <= 0) this.props.explodeBomb(this);
            else {
                this.setState({time: time});
                this.tick();
            }
        }, 1000);
        this.setState({timer: timer});
    }

    handleEvent = (e, data) => {
        this.props.handleDrop(this, data.x, data.y, this.state.color);
        return false;
    }

    render () {
        return (
            <Draggable defaultPosition={this.state.defaultPosition} onStop={this.handleEvent}>
                <div className="bomb"
                    style={{
                        backgroundColor: this.state.color,
                        height: CONSTANTS.bomb_size,
                        width: CONSTANTS.bomb_size
                    }}>
                    <span>{this.state.time}</span>
                </div>
            </Draggable>
        );
    }
}

export default Bomb;
