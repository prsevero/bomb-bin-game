import React from 'react';
import {mount} from 'enzyme';
import Draggable from 'react-draggable';
import Bomb from './bomb';


describe('Bomb Component Tests', () => {

    let props;
    let mountedBomb;
    const bomb = () => {
        if (!mountedBomb) {
            mountedBomb = mount(<Bomb {...props} />);
        }
        return mountedBomb;
    }

    beforeEach(() => {
        mountedBomb = undefined;
    });


    // All tests will go here
    it('always renders a Draggable component', () => {
        const wrapper = bomb();
        const draggable = mount(<Draggable><div /></Draggable>);
        expect(wrapper.children().name()).toEqual(draggable.name());
    });

    it('always renders a bomb', () => {
        const div = bomb().find('.bomb');
        expect(div.length).toEqual(1);
    });

    it('always renders a bomb with timer', () => {
        const timer = bomb().find('.bomb > span');
        expect(timer.length).toEqual(1);
    });

    it('always renders a bomb with text in timer', () => {
        const timer = bomb().find('.bomb > span');
        expect(timer.text()).toBeDefined();
    });

    it('always renders a bomb with text in timer greater or equal than 5', () => {
        const timer = bomb().find('.bomb > span');
        expect(parseInt(timer.text(), 0)).toBeGreaterThanOrEqual(5);
    });

    it('always renders a bomb with text in timer less or equal than 10', () => {
        const timer = bomb().find('.bomb > span');
        expect(parseInt(timer.text(), 0)).toBeLessThanOrEqual(10);
    });

    it('always renders a bomb with a color', () => {
        const div = bomb();
        expect(div.instance().state.color).toBeDefined();
    });

    it('always renders a bomb with a default position', () => {
        const div = bomb();
        expect(div.instance().state.defaultPosition).toBeDefined();
    });

    it('always renders a bomb with a time', () => {
        const div = bomb();
        expect(div.instance().state.time).toBeDefined();
    });

    it('always call time of the bomb timer', () => {
        jest.useFakeTimers();
        bomb();
        expect(setTimeout).toHaveBeenCalledTimes(1);
    });

});
