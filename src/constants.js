const COLORS = ['#d72c34', '#2e8e48', '#0778cb'];
const LIGHT_COLORS = {
    '#d72c34': 'hsl(357, 68%, {}%)',
    '#2e8e48': 'hsl(136, 51%, {}%)',
    '#0778cb': 'hsl(205, 93%, {}%)',
}
const LIGHT_COLORS_PERCENTAGES = {
    '#d72c34': 51,
    '#2e8e48': 37,
    '#0778cb': 41,
};

const BINS_TIMER = 40;
const BINS_SCALE_SIZE = 0.8;
const BINS_SCALE_FACTOR = 0.05;
const BINS_SHADE_FACTOR = 0.5;

const BOMBS_TOTAL = 120;
const BOMB_SIZE = 80;
const BOMB_SIZE_DROP = BOMB_SIZE / 2;
let BOMBS_TIMERS = [
    0, 5, 4, 4, 3.5, 3.5, 3, 3, 2.5, 2.5, 2, 2, 1.5, 1.5, 1.5, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
    0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
    0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
    0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
    0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
];
BOMBS_TIMERS = BOMBS_TIMERS.map((time) => {
    return time * 1000;
});


const CONSTANTS = {
    colors: COLORS,
    light_colors: LIGHT_COLORS,
    light_colors_percentages: LIGHT_COLORS_PERCENTAGES,
    bins_timer: BINS_TIMER,
    bins_scale_size: BINS_SCALE_SIZE,
    bins_scale_factor: BINS_SCALE_FACTOR,
    bins_shade_factor: BINS_SHADE_FACTOR,
    bombs_total: BOMBS_TOTAL,
    bombs_timers: BOMBS_TIMERS,
    bomb_size: BOMB_SIZE,
    bomb_size_drop: BOMB_SIZE_DROP,
}

export default CONSTANTS;
